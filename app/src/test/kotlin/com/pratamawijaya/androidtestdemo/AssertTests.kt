package com.pratamawijaya.androidtestdemo

import org.hamcrest.CoreMatchers.*
import org.hamcrest.core.CombinableMatcher
import org.junit.Assert.*
import org.junit.Test
import java.util.*


/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class AssertTests {

    @Test
    fun testAssertEquals() {
        assertEquals("failure - strings are not equal", "text", "text")
        assertEquals("text", "text")
    }

    @Test
    fun testAssertFalse() {
        assertFalse("failure", false)
    }

    @Test
    fun testAssertNotNull() {
        assertNotNull(Object())
    }

    @Test
    fun testAssertNotSame() {
        assertNotSame(Object(), Object())
    }

    // JUnit Matchers assertThat
    @Test
    fun testAssertThatBothContainsString() {
        assertThat("albumen", both(containsString("a")).and(containsString("b")))
    }

    @Test
    fun testAssertThatHasItems() {
        assertThat(Arrays.asList("one", "two", "three"), hasItems("one", "three"))
    }


    // Core Hamcrest Matchers with assertThat
    @Test
    fun testAssertThatHamcrestCoreMatchers() {
        assertThat("good", allOf(equalTo("good"), startsWith("good")))
        assertThat("good", not(allOf(equalTo("bad"), equalTo("good"))))
        assertThat("good", anyOf(equalTo("bad"), equalTo("good")))
        assertThat(7, not(CombinableMatcher.either(equalTo(3)).or(equalTo(4))))
        assertThat(Any(), not(sameInstance(Any())))
    }
}