package com.pratamawijaya.androidtestdemo

import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class HamcrestTest {

    @Test
    fun testWithAsserts() {
        val list = generateStingList()
        assertTrue(list.contains("android"))
        assertTrue(list.contains("context"))
        assertTrue(list.size > 4)
        assertTrue(list.size < 13)
    }

    @Test
    fun testWithBigAssert() {
        val list = generateStingList()
        assertTrue(list.contains("android")
                && list.contains("context")
                && list.size > 3
                && list.size < 12)
    }


    private fun generateStingList(): List<String> {
        return listOf("android", "context", "service", "manifest", "layout", "resource", "broadcast", "receiver", "gradle")
    }
}