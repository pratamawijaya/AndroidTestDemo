package com.pratamawijaya.androidtestdemo

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class ThermoConverterTest {

    private val ALLOWED_DELTA = 0.01f
    lateinit var thermo: ThermoConverter

    @Before
    fun setUp() {
        thermo = ThermoConverter()
    }

    @Test
    fun thermoTestLow() {
        val celciusValue = -1000f
        val thermoModel = thermo.calculateTemperature(celciusValue)

        assertEquals(celciusValue, thermoModel.celcius, ALLOWED_DELTA)
        assertEquals(-1768f, thermoModel.fahrenheit, ALLOWED_DELTA)
        assertEquals(-726.85f, thermoModel.kelvin, ALLOWED_DELTA)
    }
}