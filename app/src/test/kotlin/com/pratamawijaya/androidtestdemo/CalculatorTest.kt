package com.pratamawijaya.androidtestdemo

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class CalculatorTest {
    lateinit var calculator: Calculator

    @Before
    fun setUp() {
        calculator = Calculator()
        println("setup test")
    }

    @Test
    fun `test add number`() {
        val total = calculator.add(4, 5)
        assertEquals(9, total)
    }
}