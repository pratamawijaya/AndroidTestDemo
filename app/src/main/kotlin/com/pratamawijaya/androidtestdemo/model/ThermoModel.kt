package com.pratamawijaya.androidtestdemo.model

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
data class ThermoModel(val celcius: Float,
                       val kelvin: Float,
                       val fahrenheit: Float
)