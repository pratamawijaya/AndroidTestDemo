package com.pratamawijaya.androidtestdemo

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class Calculator {
    fun add(number1: Int, number2: Int): Any = number1 + number2
}