package com.pratamawijaya.androidtestdemo

import com.pratamawijaya.androidtestdemo.model.ThermoModel

/**
 * Created by pratama
 * Date : Jun - 6/19/17
 * Project Name : AndroidTestDemo
 */
class ThermoConverter {
    fun calculateTemperature(celsius: Float): ThermoModel {
        val fahr = celsius * 9 / 5 + 32f
        val kel = celsius + 273.15f
        return ThermoModel(celsius, kel, fahr)
    }
}